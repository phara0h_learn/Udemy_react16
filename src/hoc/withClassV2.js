import React, { Component } from 'react';

// const withClass = (WrappedContent, className) =>{
//     return (props) => (
//         <div className={className}>
//             <WrappedContent {...props} />
//         </div>
//     )
// }

const withClass = (WrappedContent, className) => {
    return class extends Component {
        render(){
            return (
                <div className={className}>
                    <WrappedContent {...this.props} />
                </div>
            )
        }
    }
};

export default withClass;