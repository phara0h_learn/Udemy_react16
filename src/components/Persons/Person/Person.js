import React, {Component} from 'react';
import PropType from 'prop-types';

import classes from './Person.css';
import Aux from '../../../hoc/_Aux';
import withClass from '../../../hoc/withClassV2';

class Person extends Component {
    componentDidMount() {
        if ( this.props.position === 0 ){
            this.inputElement.focus();
        }
    }

    render() {
        return <Aux>
                <p onClick={this.props.click}>I'm {this.props.name} and I'm {this.props.age} years old</p>
                <p>{this.props.children}</p>
                <input
                    ref={(inp) => { this.inputElement = inp } }
                    type='text'
                    onChange={this.props.changed}
                    value={this.props.name}
                />
            </Aux>; 
    }

}

Person.propTypes = {
    click: PropType.func,
    name: PropType.string,
    age: PropType.number,
    changed: PropType.func
};

export default withClass( Person, classes.Person );