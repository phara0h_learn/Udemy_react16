import React, { Component } from 'react';
import Person from './Person/Person';

class Persons extends Component { 
  render() {
    return (
      this.props.persons.map(( value, index ) => {
        return (
            <Person
              key={value.id}
              position={index}
              name={value.name}
              age={value.age}
              click={() => this.props.clicked(index)}
              changed={(event) => this.props.changed(event, value.id)}
            />
        )
      })
    )
  }
}

export default Persons;