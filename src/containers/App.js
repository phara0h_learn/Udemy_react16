import React, { Component } from 'react';
import Persons from '../components/Persons/Persons';
import Cockpit from '../components/Cockpit/Cockpit';
import classes from './App.css';
import Aux from '../hoc/_Aux';
import withClass from '../hoc/withClassV2';

class App extends Component {

    state = {
        persons: [
            { id: 'rwaf3r', name: 'Max', age: 28 },
            { id: '3rv23r', name: 'Menu', age: 26 },
            { id: '54ygh5', name: 'Steff', age: 25 }
        ],
        showPersons: false,
        toggledClicked: 0
    };

    changeNameHandler = (event, id) => {
        const personIndex = this.state.persons.findIndex(p => {
            return p.id === id;
        });

        //const person = Object.assign({}, this.state.persons[personIndex] );
        //const person = { ...this.state.persons[personIndex] };
        const persons = [...this.state.persons];
        persons[personIndex].name = event.target.value;

        this.setState({
            persons
        });
    };

    deletePersonHandler = personIndex => {
        //const persons = this.state.persons.slice();
        const persons = [...this.state.persons];
        persons.splice(personIndex, 1);

        this.setState({
            persons
        });
    };

    togglePersonsHandler = () => {
        const doesShow = this.state.showPersons;
        this.setState( (prevState, props)=>{ 
            return {
                showPersons: !doesShow,
                toggledClicked: prevState.toggledClicked + 1
            }
        });
    };

    render() {
      console.log('[App.js] - render()');
        let personsJSX = null;

        if (this.state.showPersons) {
            personsJSX = (
                <Persons
                    persons={this.state.persons}
                    clicked={this.deletePersonHandler}
                    changed={this.changeNameHandler}
                />
            );
        }

        return (
            <Aux>
                <Cockpit
                    appTitle={this.props.title}
                    persons={this.state.persons}
                    clicked={this.togglePersonsHandler}
                    showPersons={this.state.showPersons}
                />
                {personsJSX}
            </Aux>
        );
    }
}

export default withClass( App, classes.App );
